# Diagrama de entidade e relacionamento – modelagem de banco de dados
### Entidade
É basicamente todo o objeto que possua atributos que podem ser independentes ou ter uma (ou mais) interações com os outros objetos.
 - É importante toda entidade possuir pelo menos UM atributo distinguível entre outras entidades, para que <br> possa se tornar uma chave primária dentro de um  banco de dados. <br>

A chave primária também deve ter um relacionamento de registro dentro de uma tabela específica.
<br>

### Relacionamento 
É uma maneira de descrever como a entidade se relaciona diretamente com outra.
###Cardinalidade
A cardinalidade representa a quantidade de interações dentro de um diagrama ER e também pode ser identificada entre parênteses por: (0,1) (1,n) (n,1) (0,n) (n,n)

 - **Nota** : é importante ressaltar  que só é possível ter um DER em bancos de dados de modelo RELACIONAL
